PERMISSION: 
    treehelper.cut

CONFIG:
    tool: 286
    This is the tool, that you use to cut a tree. By default, it is a golden axe.
    
    mustSneaking: 0
    If is enabled (1) the player must sneaking to cut a tree.
    
    showCheckedBlocks: 0
    If is enabled (1) the console show all the checked blocks if you cut a tree.
    
    ignoreLeaves: 0
    If is enabled (1) only the tree trunk without the leaves will be destroied.
    
    range: 7
    If you have trees in a row, so you can define how many trees are destroied.