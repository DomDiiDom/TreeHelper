package net.domdiidom.treehelper.utils;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import me.confuser.barapi.BarAPI;
import net.domdiidom.treehelper.main.Main;
public class Stacks {
	private int stacks;
	private int cooldown;
	private int max_stacks;
	private boolean isCharging = false;
	Main main;
	Player player;
	@SuppressWarnings("deprecation")
	public Stacks(Main main, Player player) {
		this.main = main;
		this.player = player;
		max_stacks = main.getConfig().getInt("stacks.max");
		cooldown = main.getConfig().getInt("stacks.cooldown");
		stacks = max_stacks;
		
		Bukkit.getScheduler().scheduleAsyncRepeatingTask(main, new Runnable() {
			@Override
			public void run() {
				if(stacks < max_stacks && !isCharging) {
					onFillStacks();
					isCharging = true;
				}
			}
		}, 20, 20);
	}
	
	public void onUse() {
		stacks--;
	}
	
	@SuppressWarnings("deprecation")
	public void onFillStacks() {
		int i = stacks+1;
		BarAPI.setMessage(player, "Stacks: " + stacks + " -> "+i, cooldown);
		Bukkit.getScheduler().scheduleAsyncDelayedTask(main, new Runnable() {
			@Override
			public void run() {
				isCharging = false;
				stacks++;
			}
		}, cooldown * 20);
	}
	
	public boolean isReady() {
		if(stacks<=0)
			return false;
		else
			return true;
	}
	
	

}
