package net.domdiidom.treehelper.events;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

import net.domdiidom.treehelper.main.Main;

public class EventBlockBreak implements Listener{
	
	private Main main;
	
	boolean a = false;
	boolean b = false;
	boolean c = false;
	boolean d = false;
	boolean e = false;
	boolean f = false;
	boolean g = false;
	boolean h = false;
	boolean i = false;
	
	public EventBlockBreak(Main main) {
		this.main = main;
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onBlockBreakEvent(BlockBreakEvent event) {
		if(!event.getPlayer().hasPermission("treehelper.cut"))
			return;
		if(event.getPlayer().getItemInHand().getTypeId() != main.getConfig().getInt("tool"))
			return;
		if(!main.stacks.get(event.getPlayer().getName()).isReady())
			return;
		
		if(main.getConfig().getInt("mustSneaking") == 1) {
			if(event.getPlayer().isSneaking())
				onCheckTree(event);
		}else {
			if(!event.getPlayer().isSneaking())
				onCheckTree(event);
		}
	}
	private void onCheckTree(BlockBreakEvent event) {
		if(!isTree(event.getBlock().getLocation()))
			return;
		else {
			main.stacks.get(event.getPlayer().getName()).onUse();
			if(isBigTree(event.getBlock().getLocation())) {

				Location location = event.getBlock().getLocation();
				location.setX(location.getBlockX()+1);
				if(isTree(location)) {
					this.onCutBigTreeInRow(location);
					return;
				}
				
				location = event.getBlock().getLocation();
				location.setX(location.getBlockX()-1);
				if(isTree(location)) {
					this.onCutBigTreeInRow(location);
					return;
				}
				
				location = event.getBlock().getLocation();
				location.setZ(location.getBlockZ()+1);
				if(isTree(location)) {
					this.onCutBigTreeInRow(location);
					return;
				}
				
				location = event.getBlock().getLocation();
				location.setZ(location.getBlockZ()-1);
				if(isTree(location)) {
					this.onCutBigTreeInRow(location);
					return;
				}
							
				this.onCutBigTree(event.getBlock().getLocation());
				}else {
					
				Location location = event.getBlock().getLocation();
				location.setX(location.getBlockX()+1);
				if(isTree(location)) {
					this.onCutSimpleTreeInRow(location);
					return;
				}
				
				location = event.getBlock().getLocation();
				location.setX(location.getBlockX()-1);
				if(isTree(location)) {
					this.onCutSimpleTreeInRow(location);
					return;
				}
				
				location = event.getBlock().getLocation();
				location.setZ(location.getBlockZ()+1);
				if(isTree(location)) {
					this.onCutSimpleTreeInRow(location);
					return;
				}
				
				location = event.getBlock().getLocation();
				location.setZ(location.getBlockZ()-1);
				if(isTree(location)) {
					this.onCutSimpleTreeInRow(location);
					return;
				}
				
				this.onCutSimpleTree(event.getBlock().getLocation());
			}				
		}
	}
	
	private void onCutSimpleTreeInRow(Location location) {
		Location original = location;
		
		int x = location.getBlockX();
		int z = location.getBlockZ();
		int y = location.getBlockY();
		Block block = location.getBlock();
		while(block.getType() == Material.LOG || block.getType() == Material.LOG_2) {
			location.setY(location.getY()+1);
			block.breakNaturally();
			block = location.getBlock();
		}
			this.onDestroyLeaves(location, x, y, z, main.getConfig().getInt("range"), true);
	}
	
	
	private boolean isTree(Location location) {
		Block block = location.getBlock();
		if(block.getType() == Material.LOG || block.getType() == Material.LOG_2) {
			
			while(block.getType() != Material.AIR) {
				location.setY(location.getY()+1);
				block = location.getBlock();
				
				if(block.getType() == Material.LEAVES || block.getType() == Material.LEAVES_2) {
					return true;
				}
			}
		}else
			return false;
		
		return false;
	}
	
	private boolean isBigTree(Location location) {
		int x = location.getBlockX();
		int y = location.getBlockY();
		int z = location.getBlockZ();

		if(location.getBlock().getType() == Material.LOG || location.getBlock().getType() == Material.LOG_2) 
			e = true;
		
		location.setX(x+1);
		if(location.getBlock().getType() == Material.LOG || location.getBlock().getType() == Material.LOG_2)
			b = true;
		
		location.setZ(z+1);
		if(location.getBlock().getType() == Material.LOG || location.getBlock().getType() == Material.LOG_2)
			c = true;
		
		location.setZ(z-1);
		if(location.getBlock().getType() == Material.LOG || location.getBlock().getType() == Material.LOG_2)
			a = true;
		
		location.setX(x);
		location.setZ(z+1);
		if(location.getBlock().getType() == Material.LOG || location.getBlock().getType() == Material.LOG_2)
			f = true;
		
		location.setZ(z-1);
		if(location.getBlock().getType() == Material.LOG || location.getBlock().getType() == Material.LOG_2)
			d = true;
		
		location.setX(x-1);
		location.setZ(z);
		if(location.getBlock().getType() == Material.LOG || location.getBlock().getType() == Material.LOG_2)
		h = true;
			
		location.setZ(z+1);
		if(location.getBlock().getType() == Material.LOG || location.getBlock().getType() == Material.LOG_2)
			i = true;
		
		location.setZ(z-1);
		if(location.getBlock().getType() == Material.LOG || location.getBlock().getType() == Material.LOG_2)
			g = true;
		
		if(a && b && d && e) {
			return true;
		}
		else if(b && c && e && f) {
			return true;
		}
		else if(d && e && g && h) {
			return true;
		}
		else if(e && f && h && i) {
			return true;
		}
		else {
			return false;
		}
	}
	
	private void onReset() {
		a = false;
		b = false;
		c = false;
		d = false;
		e = false;
		f = false;
		g = false;
		h = false;
		i = false;
	}
	
	private void onCutSimpleTree(Location location) {
		Location original = location;
		
		int x = location.getBlockX();
		int z = location.getBlockZ();
		int y = location.getBlockY();
		Block block = location.getBlock();
		while(block.getType() == Material.LOG || block.getType() == Material.LOG_2) {
			location.setY(location.getY()+1);
			block.breakNaturally();
			block = location.getBlock();
		}
		if(main.getConfig().getInt("ignoreLeaves") == 0)
			this.onDestroyLeaves(location, x, y, z, 5, true);		
	}
	
	private void onDestroyLeaves(Location location, int x, int y, int z, int radius, boolean isSimpleTree) {
		int height;
		if(isSimpleTree)
			height = main.getConfig().getInt("height.simple");
		else	
			height = main.getConfig().getInt("height.big");
		x = location.getBlockX()-radius;
		y = location.getBlockY()-height;
		z = location.getBlockZ()-radius;
		
		int j = radius * 2;
		int k = radius * 2;
		
		int v = j * k * (2*height);
		
		
		location.setX(x);
		location.setY(y);
		location.setZ(z);
		
		for(int i=0; i<v; i++) {
			Block block2 = location.getBlock();

			if(main.getConfig().getInt("showCheckedBlocks") == 1)
				this.onShowCheckedBlocks(location, v, i);
			
			if(block2.getType() == Material.LOG || block2.getType() == Material.LOG_2 || block2.getType() == Material.LEAVES || block2.getType() == Material.LEAVES_2) {
				block2.breakNaturally();
			}
			
			
			if(location.getBlockX() == x+(2*radius)) {
				location.setZ(location.getZ()+1);
				location.setX(x-radius);
			}
			if(location.getBlockZ() == z+(2*radius)) {
				location.setY(location.getBlockY()+1);
				location.setX(x);
				location.setZ(z);
			}
			location.setX(location.getBlockX()+1);
		}
	}
	
	private void onShowCheckedBlocks(Location location, int v, int i) {
		Bukkit.getConsoleSender().sendMessage("�1"+location.getBlockX()+" | �2"+location.getBlockY()+" | �3"+location.getBlockZ()+" | �4"+location.getBlock().getType()+" | �5"+i+"/"+v+" Blocks Checked");
	}
	
	private void onCutBigTreeInRow(Location location) {
		Location original = location;
		int x = location.getBlockX();
		int z = location.getBlockZ();
		int y = location.getBlockY();
		
		Block block = location.getBlock();
		
		while(block.getType() == Material.LOG || block.getType() == Material.LOG_2) {
					
			if(a && b && d && e) {
				location.setX(x+1);
				location.getBlock().breakNaturally();
				location.setZ(z-1);
				location.getBlock().breakNaturally();
				location.setX(x);
				location.getBlock().breakNaturally();
				location.setZ(z);
				location.getBlock().breakNaturally();				
			}
			else if(b && c && e && f) {
				location.setX(x+1);
				location.getBlock().breakNaturally();
				location.setZ(z+1);
				location.getBlock().breakNaturally();
				location.setX(x);
				location.getBlock().breakNaturally();
				location.setZ(z);
				location.getBlock().breakNaturally();					
			}
			else if(d && e && g && h) {
				location.setZ(z-1);
				location.getBlock().breakNaturally();	
				location.setZ(z);
				location.getBlock().breakNaturally();	
				location.setX(x-1);
				location.getBlock().breakNaturally();	
				location.setZ(z-1);
				location.getBlock().breakNaturally();					
			}
			else if(e && f && h && i) {
				location.getBlock().breakNaturally();	
				location.setZ(z+1);
				location.getBlock().breakNaturally();	
				location.setX(x-1);
				location.getBlock().breakNaturally();	
				location.setZ(z);
				location.getBlock().breakNaturally();	
			}		
			location.setY(location.getY()+1);
			location.setX(x);
			location.setZ(z);
			block = location.getBlock();
		}
			this.onDestroyLeaves(location, x, y, z, main.getConfig().getInt("range"), false);	
		
		onReset();
	}
	
	private void onCutBigTree(Location location) {
		Location original = location;
		int x = location.getBlockX();
		int z = location.getBlockZ();
		int y = location.getBlockY();
		
		Block block = location.getBlock();
		
		while(block.getType() == Material.LOG || block.getType() == Material.LOG_2) {
					
			if(a && b && d && e) {
				location.setX(x+1);
				location.getBlock().breakNaturally();
				location.setZ(z-1);
				location.getBlock().breakNaturally();
				location.setX(x);
				location.getBlock().breakNaturally();
				location.setZ(z);
				location.getBlock().breakNaturally();				
			}
			else if(b && c && e && f) {
				location.setX(x+1);
				location.getBlock().breakNaturally();
				location.setZ(z+1);
				location.getBlock().breakNaturally();
				location.setX(x);
				location.getBlock().breakNaturally();
				location.setZ(z);
				location.getBlock().breakNaturally();					
			}
			else if(d && e && g && h) {
				location.setZ(z-1);
				location.getBlock().breakNaturally();	
				location.setZ(z);
				location.getBlock().breakNaturally();	
				location.setX(x-1);
				location.getBlock().breakNaturally();	
				location.setZ(z-1);
				location.getBlock().breakNaturally();					
			}
			else if(e && f && h && i) {
				location.getBlock().breakNaturally();	
				location.setZ(z+1);
				location.getBlock().breakNaturally();	
				location.setX(x-1);
				location.getBlock().breakNaturally();	
				location.setZ(z);
				location.getBlock().breakNaturally();	
			}		
			location.setY(location.getY()+1);
			location.setX(x);
			location.setZ(z);
			block = location.getBlock();
		}
		if(main.getConfig().getInt("ignoreLeaves") == 0)
			this.onDestroyLeaves(location, x, y, z, 5, false);	
		
		onReset();		
	}

}
