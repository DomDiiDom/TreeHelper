package net.domdiidom.treehelper.events;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import net.domdiidom.treehelper.main.Main;
import net.domdiidom.treehelper.utils.Stacks;

public class EventPlayerJoin implements Listener{
	Main main;
	public EventPlayerJoin(Main main) {
		this.main = main;
	}
	
	@EventHandler
	public void onEvent(PlayerJoinEvent event) {
		if(!main.stacks.containsKey(event.getPlayer().getName())) {
			main.stacks.put(event.getPlayer().getName(), new Stacks(main, event.getPlayer()));
		}
	}

}
