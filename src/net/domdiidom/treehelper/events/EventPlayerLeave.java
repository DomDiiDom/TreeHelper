package net.domdiidom.treehelper.events;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import net.domdiidom.treehelper.main.Main;

public class EventPlayerLeave implements Listener{
	Main main;
	public EventPlayerLeave(Main main) {
		this.main = main;
	}
	
	@EventHandler
	public void onEvent(PlayerQuitEvent event) {
		if(main.stacks.containsKey(event.getPlayer().getName())) {
			main.stacks.remove(event.getPlayer().getName());
		}
	}
}
