package net.domdiidom.treehelper.main;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import net.domdiidom.treehelper.events.EventBlockBreak;
import net.domdiidom.treehelper.events.EventPlayerJoin;
import net.domdiidom.treehelper.events.EventPlayerLeave;
import net.domdiidom.treehelper.utils.Stacks;

public class Main extends JavaPlugin{
	
	public HashMap<String, Stacks> stacks = new HashMap<String, Stacks>();
	private ConsoleCommandSender console = Bukkit.getConsoleSender();
	
	@Override
	public void onEnable() {
		console.sendMessage("�2[TreeHelper] Enabled");
		
		this.onRegisterEvents();
		this.onLoadConfig();
		this.onLoadPlayers();
	}
	
	@Override
	public void onDisable() {
		console.sendMessage("�4[TreeHelper] Disabled");
	}

	public void onRegisterEvents() {
		this.getServer().getPluginManager().registerEvents(new EventBlockBreak(this), this);
		this.getServer().getPluginManager().registerEvents(new EventPlayerJoin(this), this);
		this.getServer().getPluginManager().registerEvents(new EventPlayerLeave(this), this);
	}
	
	public void onLoadConfig() {
		this.getConfig().options().copyDefaults(true);
		this.saveConfig();
	}
	
	public void onLoadPlayers() {
		stacks.clear();
		
		for(Player player : Bukkit.getOnlinePlayers()) {
			if(!stacks.containsKey(player)) {
				stacks.put(player.getName(), new Stacks(this, player));
			}
		}
	}
}
